import './vendor';
import './location';

/** owl-carousel **/
/* owl-special-offers */
$(document).ready(() => {
	let owl = $('.owl-carousel.owl-special-offers');
	let owlWrapper = owl.parent('.owl-carousel-wrapper');
	let currentOffer = owlWrapper.find('span.current-offer');

	owl.on('initialized.owl.carousel', (event) => {
		owlWrapper.find('span.length-offer').html(event.item.count);
	});

	owl.owlCarousel({
		items: 1,
		margin: 25,
		smartSpeed: 500,
		dots: false,
		loop: true,
		responsive: {
			768: {
				autoplay: true,
				autoplayTimeout: 4000,
				autoplayHoverPause: true,
			},
		},
	});

	owl.on('changed.owl.carousel', (event) => {
		if (event.item.index === 0 || event.item.index - Math.floor(event.item.count / 2 - 1) > event.item.count) {
			currentOffer.html(1);
		} else if (event.item.index === Math.floor(event.item.count / 2 - 1)) {
			currentOffer.html(event.item.count);
		} else {
			currentOffer.html(event.item.index - Math.floor(event.item.count / 2 - 1));
		}

		// if (event.item.index === 2) {
		// 	currentOffer.html(event.item.count);
		// } else if (event.item.index - 2 > event.item.count) {
		// 	currentOffer.html(1);
		// } else {
		// 	currentOffer.html(event.item.index - 2);
		// }
	});

	owlWrapper.find('.btn-next').click(() => {
		owl.trigger('next.owl.carousel', [700]);
	});

	owlWrapper.find('.offer-nav').hover(
		() => {
			owl.trigger('stop.owl.autoplay');
		}
		,
		() => {
			owl.trigger('play.owl.autoplay', [6000]);
		}
	);

	owlWrapper.find('.btn-prev').click(() => {
		owl.trigger('stop.owl.autoplay');
		owl.trigger('prev.owl.carousel', [700]);
	});
});
/* END owl-special-offers */

/* owl-menu */
$(document).ready(() => {
	let owlMenu = $('.owl-carousel.owl-menu.owl-menu-combo');
	let owlMenuWrapper = owlMenu.parent('.owl-carousel-wrapper');
	let currentOffer = owlMenuWrapper.find('span.current-offer');

	owlMenu.on('initialized.owl.carousel', (event) => {
		owlMenuWrapper.find('span.length-offer').html(event.item.count);
	});

	owlMenu.owlCarousel({
		items: 1,
		margin: 25,
		smartSpeed: 500,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		loop: true,
		dots: false,
	});

	owlMenu.on('changed.owl.carousel', (event) => {
		if (event.item.index === 0 || event.item.index - Math.floor(event.item.count / 2) > event.item.count) {
			currentOffer.html(1);
		} else if (event.item.index === Math.floor(event.item.count / 2)) {
			currentOffer.html(event.item.count);
		} else {
			currentOffer.html(event.item.index - Math.floor(event.item.count / 2));
		}
	});

	owlMenuWrapper.find('.btn-next').click(() => {
		owlMenu.trigger('next.owl.carousel', [700]);
	});

	owlMenuWrapper.find('.btn-prev').click(() => {
		owlMenu.trigger('prev.owl.carousel', [700]);
	});
});
/* END owl-menu */

/* owl-menu */
$(document).ready(() => {
	let owlMenu = $('.owl-carousel.owl-menu.owl-menu-coffee');
	let owlMenuWrapper = owlMenu.parent('.owl-carousel-wrapper');

	owlMenu.on('initialized.owl.carousel', (event) => {
		owlMenuWrapper.find('span.length-offer').html(event.item.count);
	});

	owlMenu.owlCarousel({
		items: 1,
		margin: 25,
		smartSpeed: 500,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
	});

	owlMenu.on('changed.owl.carousel', (event) => {
		if (event.item.index === 2) {
			owlMenuWrapper.find('span.current-offer').html(event.item.count);
		} else {
			owlMenuWrapper.find('span.current-offer').html(event.item.index - 2);
		}
	});

	owlMenuWrapper.find('.btn-next').click(() => {
		owlMenu.trigger('next.owl.carousel', [700]);
	});

	owlMenuWrapper.find('.btn-prev').click(() => {
		owlMenu.trigger('prev.owl.carousel', [700]);
	});
});
/* END owl-menu */

/* owl-about */
$(document).ready(() => {
	let owlAbout = $('.owl-carousel.owl-about');

	owlAbout.owlCarousel({
		items: 1,
		margin: 25,
		smartSpeed: 500,
		dots: false,
		nav: true,
		navText: ['<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADFwAAAxcBwpsE1QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAD7SURBVGiB7dk9DgFRGIXhG5XEClTWQEnHCuwFhURQYzVqe7EBCZKpX4W/ZMwtvpnEucP3dFOQ8yaSuWZCcM45ZwB0gDHQUm8pDRgAF+726j2lPCKuvGXqTWYFEQBz9S4TYAhkuYiJepdJJGKq3mXyKxGjgoiZepeJR6QiErFU7zKJRKzUu0w8IhWRiLV6l4lHpALo83mK3ap3mQHHXMROtaVR8fPkrs8Vv08j8tNaqHeVAvSAUy5mo95Visek6h9i6ndvCeH3YroekyKPSVUkRnbIrMRjUkXxa4V6PQd+Koip53+ZEF4x50fIQb2nEqDN/ZVDU73FOee+5wawKRICw7estAAAAABJRU5ErkJggg==">', '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADFwAAAxcBwpsE1QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAADbSURBVGiB7dk7CsJAFIXhwUpwAzYuwlLLlK7HR+er9bEcS5fiDgIhYv1bKIhjZSLcueP5umnC+SHFwIQgIvI3gB4wAQbWW1oBTjxUwNh6T2NAyUvtNgaY8s51zCyKuQGF9a5GcouZKyZFucWscopZKyZFucVsFJOi3GK2OcUcopgaGFnvagQ4RjGXNt/r/GpYA1V0xmRFG8DS/a8F7KKIEhha7/oKsFdEChSRCkWkgs8riCJMKCIVuUTE13GXEYsowuezAnB1HxFCCMD5GeH+6a0LFEDfeouIiC93OHQR8Jz8JwgAAAAASUVORK5CYII=">'],
		responsive: {
			768: {
				items: 3,
			},
		},
		loop: true,
	});
});
/* END owl-about */

/* owl-menu-mobile */
$(document).ready(() => {
	let $owlMenuMob = $('.owl-menu-mobile');

	function startOwlMenuMobile() {
		$owlMenuMob.addClass('owl-carousel');
		$owlMenuMob.owlCarousel({
			items: 1,
			margin: 25,
			smartSpeed: 500,
			nav: true,
			dots: false,
			loop: true,
			navText: ['<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB3SURBVEhL7Y3BDUBAEEVXOFMA3VCUGlwcXFSgEppQhnDmzcZBAzsb2XnJy+TP5TnjrxTvVaXFAwe/lJDohTf28tDgGx0xw+BYNCgWDUqUaIUnSnRGlaiQ44oS3rFBNUrc0OLpxmtUw+LR44s8NJH4hJ1fRuI49wBaqTmbN42i3gAAAABJRU5ErkJggg==">', '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB2SURBVEhL7c9BCkBQFIXhp6RMDEztjbICy7AHy5E9yJhNSPhvMTdxGZy/vl7dyekF9bfi63UtxYIemR28SjDiwADX8QITNK5xlzT+2fgMG+/s4FWFDTtqO3hU4h5t7OCRRl9No6/2yWiOFa6jVoQW9mulHhbCCeZvOaH0XJ0bAAAAAElFTkSuQmCC">'],
		});
	}

	if ($(window).width() <= '767') {
		startOwlMenuMobile();
	}
});
/* END owl-menu-mobile */

/** END owl-carousel **/

$(document).ready(() => {
	$('.menu-item').hover(function () {
		$(this).toggleClass('show');
	});
});

/* modal */
$('button[data-modal], a[data-modal]').on('click touchstart', (event) => {
	let nameModal = $(event.target).closest('[data-modal]').attr('data-modal');

	if (nameModal === 'all-address' && $(window).width() < 768) {
		nameModal = 'all-address-mobile';
	}

	$(`#${nameModal}`).modal({
		fadeDuration: 130,
	});

	return false;
});

/* END modal */

/* Плавный скролл по якорям */
$(document).ready(() => {
	$('.nav ul a, .footer__nav a').on('click', function (event) {
		event.preventDefault();
		let attrModal = $(this).attr('data-modal');

		if (typeof attrModal !== typeof undefined) {
			return;
		}

		let id = $(this).attr('href');

		let top = $(id).offset().top + 30;

		$('body,html').animate({scrollTop: top}, 350);
	});
});

/* END Плавный скролл по якорям */

/* Кнопка "Подробнее" */
$(document).ready(() => {
	$('.read-more').click((event) => {
		let btnReadMore = $(event.target);

		if (btnReadMore.hasClass('isOpen')) {
			btnReadMore.prev().slideUp();
			btnReadMore.insertBefore(btnReadMore.prev());
			btnReadMore.removeClass('isOpen');
		} else {
			btnReadMore.next().slideDown();
			btnReadMore.insertAfter(btnReadMore.next());
			btnReadMore.addClass('isOpen');
		}
	});
});

/* END Кнопка "Подробнее" */

/* Кнопка меню навигации */
$('.menu-toggle').on('click', function () {
	$(this).toggleClass('on');
	$('.menu-section').toggleClass('on');
	$('nav ul').toggleClass('hidden');
});

/* END Кнопка меню навигации */

/* Скачать меню */
$('button[data-file], a[data-file]').on('click', (event) => {
	let fileName = event.target.dataset.file;

	window.open(`./${fileName}`);
});

/* END Скачать меню */

/* Решение проблемы с прыгающей высотой блоков (vh) в мобильных браузерах */
let vh = window.innerHeight * 0.01;

document.documentElement.style.setProperty('--vh', `${vh}px`);

/* END Решение проблемы с прыгающей высотой блоков (vh) в мобильных браузерах */

/* Решение проблемы с открытием ссылки на iPhone */
$(document).on('click', '.link-old_mobile a', function () {
	window.open($(this)[0].href);
});

/* END Решение проблемы с открытием ссылки на iPhone */

import '@babel/polyfill';
import svg4everybody from 'svg4everybody';
import './vendor/checkWebp.min';
import 'owl.carousel';
import 'jquery-modal';

svg4everybody();

require('ninelines-ua-parser');

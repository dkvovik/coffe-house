/*eslint-disable*/
if (typeof ymaps !== 'undefined') {
	ymaps.ready(init);
}

function init() {
	var
		myMap = new ymaps.Map("map", {
				center: [55.76, 37.64],
				zoom: 12,
				controls: ['zoomControl']
			}
		),
		collection = new ymaps.GeoObjectCollection(null, { preset: groups.style });

	myMap.behaviors.disable('scrollZoom');

	if (document.documentElement.classList.contains('is-device-mobile')) {
		myMap.behaviors.disable('drag');
	}

	myMap.geoObjects.add(collection);

	for (var j = 0, m = groups.items.length; j < m; j++) {
		createMenuGroup(groups.items[j])
	}

	function createMenuGroup (item) {
		var placemark = new ymaps.Placemark(item.center,
				{
					balloonContent: `<b>${item.address}</b><br>`
				},
				{
					iconLayout: 'default#image',
					iconImageHref: 'images/map_icon.svg',
					iconImageSize: [35, 49],
					iconImageOffset: [-22, -55]
				});

		// Добавляем метку в коллекцию.
		collection.add(placemark)
	}
}

var groups = {
	name: 'Известные памятники',
	style: 'islands#redIcon',
	items: [
		{
			center: [55.719455, 37.608266],
			address: 'Москва, улица Шаболовка, 30/12',
		},
		{
			center: [55.794445, 37.588248],
			address: 'Москва, Савеловский вокзала, д. 2',
		},
		{
			center: [55.793586, 37.615246],
			address: 'Москва, Сущевский Вал д. 59',
		},
		{
			center: [55.682651, 37.661913],
			address: 'Москва, Андропова пр-кт, д. 22',
		},
		{
			center: [55.708780, 37.733006],
			address: 'Волгоградский пр-т, д. 46/15, стр. 3А',
		},
		{
			center: [55.684503, 37.621947],
			address: 'Варшавское шоссе, д. 26, стр 5.7.8 (Кофе Хауз Киоск)',
		},
		{
			center: [55.737532, 37.590138],
			address: 'Москва, Зубовский бульвар, д. 4, 2-й этаж',
		},
		{
			center: [55.737532, 37.590138],
			address: 'Москва, Зубовский бульвар, д. 4, 6-й этаж (РИА Новости)',
		},
		{
			center: [55.735961, 37.591701],
			address: 'Москва, Зубовский бульвар, д. 15, стр. 1',
		},
		{
			center: [55.750712, 37.596148],
			address: 'Арбат, д 19 (Шоколадный киоск)',
		},
		{
			center: [55.757491, 37.660808],
			address: 'Земляной Вал, д. 29 (Шоколадный киоск)',
		},
		{
			center: [55.729741, 37.639455],
			address: 'Москва, Павелецкая площадь, д. 1А, стр. 1',
		},
		{
			center: [55.798616, 37.939861],
			address: 'МО, Балашиха, Парковая ул., д. 2',
		},
		{
			center: [55.757491, 37.635404],
			address: 'Москва, Маросейка 6/8 стр. 1',
		},
		{
			center: [55.734151, 37.618920],
			address: 'Б. Полянка, д. 30',
		},
		{
			center: [55.708284, 37.652847],
			address: 'Ленинская Слобода д. 19, БЦ Омега Плаза',
		},
		{
			center: [55.705402, 37.656424],
			address: 'Автозаводская д. 13 (Кофе Хауз Киоск)',
		},
		{
			center: [55.773611, 37.655670],
			address: 'Москва, Комсомольская площадь, д. 2 (Казанский вокзал, 5-ый подъезд)',
		},
		{
			center: [55.773448, 37.655331],
			address: 'Москва, Комсомольская площадь, д. 2 (Казанский вокзал, платформа пригород)',
		},
		{
			center: [55.773448, 37.655331],
			address: 'Москва, Комсомольская площадь, д. 2 (Казанский вокзал, подъезд 2)',
		},
		{
			center: [55.773448, 37.655331],
			address: 'Москва, Комсомольская площадь, д. 2 (Казанский вокзал, павильон 23)',
		},
		{
			center: [55.776475, 37.657736],
			address: 'Комсомольская площадь, д. 5 (Ярославский вокзал)',
		},
		{
			center: [55.776127, 37.655347],
			address: 'Комсомольская пл., д. 3 (Кофе Хауз Киоск)',
		},
		{
			center: [55.966339, 37.415885],
			address: 'Москва, аэропорт Шереметьево, терминал B',
		},
		{
			center: [55.781818, 37.598690],
			address: 'Москва, м. Менделеевская, подуличный переход ',
		},
	]
};
